﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MongoDataModify
{
    [BsonIgnoreExtraElements]
    public class Game
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("ProductName")]
        public string Name { get; set; }

        public int Views { get; set; }

        public string GameKey { get; set; }
    }
}
