﻿using MongoDB.Driver;
using System;

namespace MongoDataModify
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("MongoDb Northwind data modifying...");

            MongoClient client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("Northwind");
            var collection = database.GetCollection<Game>("products");
            var data = collection.Find(Builders<Game>.Filter.Empty).ToList();

            foreach (var item in data)
            {
                var filter = Builders<Game>.Filter.Eq(d => d.Id, item.Id);
                var update = Builders<Game>.Update
                    .Set(d => d.Views, 0)
                    .Set(d => d.GameKey, item.Name.Trim().ToLower()
                        .Replace(" ", "_").Replace("\'", ""));
                collection.UpdateOne(filter, update);
            }

            

            Console.WriteLine("Data was updated!");
        }
    }
}
